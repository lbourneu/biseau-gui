"""Tutorial to teach the basics of biseau.

"""


TUTORIAL_NAME = 'Biseau basics'
ORDERED_TUTO = (
"""
% Biseau is a python package providing a complete tooling
%  for ASP to graph compilation.

% ASP, standing for Answer Set Programming,
%  is a declarative programming language (such as Prolog)
%  that enables a really close-to-specification encoding
%  of problems.

% The basic principles rely on a controlled vocabulary
%  that describes the graph content.

% Here is an example:
link(a,b).  % we expect you to know ASP basics.
$run
$focus Graph

% This tutorial consider that you understand the ASP basics,
%  and explore the basics of biseau's usage.

% For a written documentation, please look at:
%  https://gitlab.inria.fr/lbourneu/biseau/blob/master/doc/user-doc.mkd

% Hit the next button to start this tutorial.

$end
""", #"""
#% 

#"""
)
# """, """
