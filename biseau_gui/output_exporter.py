"""Interface to choose which output to export.

"""

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

class OutputExporterDialog(QDialog):
    """Help user to define the exported data"""


    def __init__(self, parent):
        super().__init__(parent)
        self._widgets = {}
        self.build_widgets({
            'save in': (QLineEdit, 'out-{tab}', "Basename for output files. Can be a path.\nUse '{tab}' as a placeholder for the tab number\n(useful when having one result for each model)"),
            'PNG': (QCheckBox, True, "Check to get a png output"),
            'SVG': (QCheckBox, True, "Check to get a vector graphics output"),
            'dot': (QCheckBox, True, "Check to get the final dot"),
            'ASP': (QCheckBox, True, "Check to get the full ASP code"),
        })

    def build_widgets(self, options):
        "Build widgets of the dialog"
        def make_labelled(label:str, widget:QWidget, tooltip:str) -> QHBoxLayout:
            h_layout = QHBoxLayout()
            lab = QLabel(label, parent=self)
            h_layout.addWidget(lab)
            h_layout.addWidget(widget)
            lab.setToolTip(tooltip)
            widget.setToolTip(tooltip)
            return h_layout

        # create widgets, put them in the layout
        layout = QVBoxLayout()
        for name, (widtype, default, tooltip) in options.items():
            wid = widtype(parent=self)
            if widtype is QLineEdit:
                wid.setText(default)
            elif widtype is QCheckBox:
                wid.setCheckState(Qt.Checked if default else Qt.Unchecked)
            else:
                raise NotImplementedError(f"widget type {widtype}.")
            layout.addLayout(make_labelled(name, wid, tooltip))
            self._widgets[name] = wid

        # buttons
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        )
        self.button_box.accepted.connect(self.make_export)
        self.button_box.rejected.connect(self.reject)
        layout.addWidget(self.button_box)

        self.setLayout(layout)
        self.setWindowTitle("Output exporter")


    @property
    def out_filename(self) -> str:
            return self._widgets['save in'].text()
    @property
    def out_png(self) -> bool:
            return self._widgets['PNG'].checkState() is Qt.Checked
    @property
    def out_svg(self) -> bool:
            return self._widgets['SVG'].checkState() is Qt.Checked
    @property
    def out_dot(self) -> bool:
            return self._widgets['dot'].checkState() is Qt.Checked
    @property
    def out_asp(self) -> bool:
            return self._widgets['ASP'].checkState() is Qt.Checked


    def make_export(self):
        self.parent().export_outputs(self.out_filename, png=self.out_png, svg=self.out_svg, dot=self.out_dot, asp=self.out_asp)
        self.accept()  # close the dialog successfully
