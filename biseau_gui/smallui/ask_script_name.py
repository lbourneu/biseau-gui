import sys
from PySide2.QtWidgets import (QLineEdit, QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QDialog)


class AskScriptName(QDialog):
    """Simple dialog asking user to get a script name"""

    def __init__(self, parent, current_name:str, callback:callable):
        super().__init__(parent)
        self.setWindowTitle('Script name editor')
        self.callback = callback
        # Create widgets
        self.labl = QLabel("Name:")
        self.edit = QLineEdit(current_name)
        self.but_no = QPushButton("Cancel")
        self.but_ok = QPushButton("Rename")
        # Create layout and add widgets
        text_layout = QHBoxLayout()
        text_layout.addWidget(self.labl)
        text_layout.addWidget(self.edit)
        but_layout = QHBoxLayout()
        but_layout.addWidget(self.but_no)
        but_layout.addWidget(self.but_ok)
        vlayout = QVBoxLayout()
        vlayout.addLayout(text_layout)
        vlayout.addLayout(but_layout)
        # Set dialog layout
        self.setLayout(vlayout)
        # Add button signal to greetings slot
        self.but_ok.clicked.connect(lambda: (self.callback(self.edit.text()), self.accept()))
        self.but_no.clicked.connect(self.reject)
