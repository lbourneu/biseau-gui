
metabolic-network:
	python -m biseau_gui -l metabolic-network
asp-tuto:
	python -m biseau_gui -l tuto-ASP-basics
py-tuto:
	python -m biseau_gui -l tuto-pyscript-basics
run:
	python -m biseau_gui -l FCA
run-init:
	python -m biseau_gui -l init
run-empty:
	python -m biseau_gui


styling: black
black:
	black *.py


install-deps:
	python -c "import configparser; c = configparser.ConfigParser(); c.read('setup.cfg'); print(c['options']['install_requires'])" | xargs pip install -U
fullrelease:
	fullrelease
