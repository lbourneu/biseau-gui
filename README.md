# biseau-gui

A GUI for [biseau](https://gitlab.inria.fr/lbourneu/biseau).

    pip install biseau-gui


## Main Features

- real-time rendering of graphs.
- full support of biseau features: dot visualization, scripts, exports.
- python and ASP code edition.
- interactive tutorials: learn about ASP and biseau with incremental exercises.
